<div class="col-xs-12 col-sm-12">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="conts oculto" data-user="<?= $user_id ?>"></div>
            <div class="alert alert-success oculto" role="alert">
                <!--<p>Especificaciones del curso</p>-->
                <section class="especificaciones-curso ">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    Identificador
                                </div>
                                <div class="col-xs-12 col-sm-6" id="id-curso">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    Nombre
                                </div>
                                <div class="col-xs-12 col-sm-6" id="nombre-curso">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    Fecha de inicio
                                </div>
                                <div class="col-xs-12 col-sm-6" id="fecha-inicio-curso">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    Fecha de finalización
                                </div>
                                <div class="col-xs-12 col-sm-6" id="fecha-fin-curso">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            
            <?= $this->load->view('home/eventos/formBusqueda', '',TRUE);?>

            <section class="row oculto datos-eventos" id="boton-agregar-evento">
                <div class="col">
                    <div class="dropdown-divider"></div>
                    <button type="button" class="btn btn-primary disparador-modal agregar-evento" data-toggle="modal" data-target="#modal-form-eventos">
                        Agregar evento 
                    </button>
                </div>
            </section>

            <?= $this->load->view('home/eventos/formEvento', '',TRUE);?>
            <!--Aquí van los eventos de actividad/curso-->
            <?= $this->load->view('home/eventos/eventosActividad', '',TRUE);?>
            <?= $this->load->view('home/eventos/eventosCurso', '',TRUE);?>

        </div>
    </div>
</div>