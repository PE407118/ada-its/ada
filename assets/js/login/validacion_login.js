$("#login-form").validate({
    rules: {
        email: {

            required: true,
            minlength: 20
        },
        password: {
            required: true,
            minlength: 6
        }
    },
    messages: {

        email: {
            required: 'Este campo es obligatorio',
            minlength: 'Ingresa tu correo de moodle'
        }, 
        password: {
            required: 'Este campo es obligatorio',
            minlength: 'Ingresa tu contraseña de moodle'
        }
    }
});