<div class="modal fade" id="modal-form-eventos" tabindex="-1" role="dialog" aria-labelledby="modal-form-eventos-label"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-form-eventos-label">Programación de Reglas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="evento-form">
                <div class="modal-body" id="registro-evento-form" >
                        
                    <div class="row">
                        <div class="col-xs-12 col-sm-12">

                            <small>Para agregar reglas da clic en el botón de "Agregar", según el criterio de
                                programación del evento.</small>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12">
                            <div class="alert alert-warning mi-warning oculto" role="alert">

                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12">
                            <div class="alert alert-danger mi-danger oculto" role="alert">

                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-xs-12 col-sm-12">
                            <div class="alert alert-success mi-success oculto" role="alert">

                            </div>
                            <br>
                        </div>
                    </div>

                    <div class="row oculto">
                        <div class="col-xs-12 col-sm-12">
                            <br>
                        </div>
                    </div>

                    <div class="form-row ">
                        <input type="hidden" value="" name="evento_id" id="evento_id">
                        <input type="hidden" value="" name="status" id="status">

                        <div class="form-group col-md-6" id="reglas-evento">
                            <label>Regla</label>
                            <div class="combo">
                                <select id="reglas-evento" class=" selectize form-control" name="regla_id">
                                    <option value="" selected="selected" disabled="disable">No disponible</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-6" id="mensaje_regla">
                            <label>Mensaje</label>
                            <textarea class="form-control" id="mensaje" rows="4" name="mensaje"></textarea>
                        </div>
                    </div>

                    <!--
                        <article class="form-row alertas oculto" id='advertencia-paginas-totales'>
                            <div class="col-xs-12 col-sm-12">
                                <div class="alert alert-warning" >
                                    Modificar el número de páginas totales del curso para este evento modificará dicho valor para el resto de eventos programados para este curso.
                                </div>
                            </div>
                        </article>

                        <div class="form-row paginas_totales_curso oculto" id="paginas_totales_curso">
                            <div class="form-group form-inline">
                                
                                <label class="my-1 mr-2">Páginas totales del curso</label>
                                <input type="text" class="my-1 mr-sm-1" size="5" name="paginas_totales_curso"
                                    id="paginas_totales_curso" >
                            </div>
                        </div>
                    
                    <div class="form-row mi_contenido " id="mi_contenido">
                        <div class="form-group form-inline col-md-12">
                            <label class="my-1 mr-2">URL de la página de avance</label>
                            <input type="text" class="form-control" name="contenido_tiempo" id="contenido_tiempo"
                                placeholder="url de la página del curso en moodle">
                            <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample"
                                aria-expanded="false" aria-controls="collapseExample">
                                <i class="fa fa-question-circle-o fa-3x" aria-hidden="true"></i>Ayuda</a>
                            <div class="collapse" id="collapseExample">
                                <div class="well">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur voluptas ipsum adipisci unde, debitis, nemo odit blanditiis laudantium quae aspernatur, hic magni optio ex corporis necessitatibus perspiciatis aliquam omnis cumque.</p>
                                </div>
                            </div>
                        </div>
                    </div>-->

                    <div class="form-row ponderacion oculto" id="ponderacion">
                        <div class="form-group form-inline">
                            <label class="my-1 mr-2">Ponderación de</label>
                            <input type="text" class="my-1 mr-sm-1" size="5" name="porcentaje_ponderacion"
                                id="porcentaje_ponderacion" placeholder="80">
                            <label class="my-1 mr-2">% del 100% </label>
                        </div>
                    </div>

                    <div class="form-row combo-uapas oculto">
                        <div class="form-group col-md-6" id="uapas-evento">
                            <label>Uapas</label>
                            <div class="combo">
                                <select id="uapas-evento-select" class="selectize form-control" name="uapa_id">
                                    <option selected="selected" disabled="disabled">No disponible</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    
                    <article class="form-row alertas oculto" id='advertencia-vigencia-grupo'>
                        <div class="col-xs-12 col-sm-12">
                            <div class="alert alert-warning">
                                Modificar la fecha de inicio o fin de vigencia del grupo cambiará la fecha de
                                ejecución a todos los eventos que la consideren en su funcionamiento
                            </div>
                        </div>
                    </article>

                    <div class="form-row fechas" id="fecha_programacion">
                        <div class="form-group col-md-3">
                            <label>Fecha límite</label>
                        </div>

                        <div class="input-group date col-md-9" id="fin_fecha">
                            <input type="text" id='fecha_fin' class="form-control fecha-evento" size="70"
                                name="fecha_fin" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="form-row col-md-12 oculto" id="tiempo_antes">
                        <div class="col-md-4 " style="padding:3%;">
                            <input type="number" name="cantidad_tiempo" min="1" max="23">
                        </div>
                        <div class="col-md-4">
                            <div class="form-check form-check-inline miradio">
                                <input class="form-check-input" type="radio" name="unidad_tiempo" id="unidad_tiempo"
                                    value="D" checked>
                                <label class="form-check-label" for="unidad_tiempo">Día</label>
                            </div>
                            <div class="form-check form-check-inline miradio">
                                <input class="form-check-input" type="radio" name="unidad_tiempo" id="unidad_tiempo"
                                    value="H">
                                <label class="form-check-label" for="unidad_tiempo">Hora</label>
                            </div>
                        </div>
                        <div class="col-md-4" style="padding:3%;">
                            <label  > Antes</label>
                        </div>
                    </div>

                    <div class="form-row col-md-12">
                        <div class="form-group col-md-3">
                            <p>Medio para mostrar los eventos</p>
                        </div>
                        <div class="form-group col-md-9">
                            <div class="form-check form-check-inline miradio">
                                <input class="form-check-input" type="radio" name="medio_envio" id="env_ada"
                                    value="ADA">
                                <label class="form-check-label" for="env_ada">ADA</label>
                            </div>
                            <div class="form-check form-check-inline miradio">
                                <input class="form-check-input" type="radio" name="medio_envio" id="env_email"
                                    value="Email">
                                <label class="form-check-label" for="env_email">Email</label>
                            </div>
                            <div class="form-check form-check-inline miradio">
                                <input class="form-check-input" type="radio" name="medio_envio" id="env_ambos"
                                    value="Ambos">
                                <label class="form-check-label" for="env_ambos">Ambos</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary enviar-info-evento">Agregar</button>
                    <button type="button" class="btn btn-warning oculto cambia-status-evento">Desactivar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </form>
        </div>
    </div>
</div>