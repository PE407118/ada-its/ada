	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (verifica_sesion() !==true){
			$this->session->error ='Favor de iniciar sesión';
			redirect();
		}
	}
	
	public function ada()
	{
		$data = array( 
			'item_menu' => 'ada'
		);
        $this->layout->setLayout('home');
        $this->layout->view('ada',$data);
	}

/*
| -------------------------------------------------------------------
| ->Metodo: user().[Sin parametros]
| -------------------------------------------------------------------
| Muestra una vista con la informacion (en forma de tabla) de los usuarios que
| se encuentran registrados en la base de datos Moodle y que cuentan con
| un rol asignado en la plataforma de Moodle.
| -------------------------------------------------------------------
*/
	 public function user()
	{
		//cargamos la base de datos de moodle
		$this->load->database('moodle');
		//establecemos el layout 'home', (navbar)
		$this->layout->setLayout('home');
		//cargamos el modelo "Usuarios.php" con los metodos que contienen consultas SQL
		$this->load->model('Usuarios');
		$losusuarios = $this->Usuarios->get_users();
		$data =array(
			'losusuarios'=> $losusuarios,
			'item_menu'=>'usuarios'
		);
		//cargamos la vista "usuarios", le enviamos los datos de la consulta
		//en un objeto de nombre "losusuarios"
		$this->layout->view('usuarios',$data);
	} 
/*
| -------------------------------------------------------------------
| ->Metodo: registro().[Recibe el parametro $id, id del usuario en Moodle]
| -------------------------------------------------------------------
| Muestra una formulario inabiilitado para edicion, muestra los datos del
| usuario seleccionado, estos datos fueron extraidos por el metodo user()
| que se encuentran registrados en Moodle y que cuentan con un rol asignado.
| Esta funcion recibe como parametro un entero que es el id del usuario
| extraido de la tabla mdl_user.
| El envio de este parametro se realiza automaticamente de la vista
| -------------------------------------------------------------------
*/                      
	public function registro($id){
		//cargamos la base de datos de moodle
		$this->load->database('moodle');
		//establecemos el layout 'home', (navbar)
		$this->layout->setLayout('home');
		//cargamos el modelo "Usuarios.php" con los metodos que contienen consultas SQL
		$this->load->model('Usuarios');
		//almacenamos los datos consultados en masusuarios
		//se llama al metodo registro enviandole a la consulta SQL el valor entero del 
		//id del usuario seleccionado en la vista usuarios
		$masusuarios = $this->Usuarios->registro($id);
		//cargamos la vista "usuarios", le enviamos los datos de la consulta
		//en un objeto de nombre "masusuarios"
		$this->layout->view('registro',compact("masusuarios",array('item_menu' => 'usuarios')));
	}
/*
| -------------------------------------------------------------------
| ->Metodo: altaAda().[Recibe]
| -------------------------------------------------------------------
| 
| -------------------------------------------------------------------
*/ public function altaAda(){
	//cargamos la base de datos de ada (ada_front_end)
	$this->load->database('ada');
	//cargamos el modelo "Usuarios.php" con los metodos que contienen consultas SQL
	$this->load->model('Usuarios');
	//guardamos en una variable los datos capturados por el metodo post del formulario
	
	$id_user = $this->db->escape($_POST["id"]);
	$status = $this->db->escape($_POST["status"]);
	$timecreated = $this->db->escape($_POST["update_date"]);
	$update_date = $this->db->escape($_POST["update_date"]);
	$id_role = $this->db->escape($_POST["tipo_user_id"]);
	
	
	//almacenamos los datos consultados en masusuarios
	//se llama al metodo altaAda enviandole a la consulta SQL el valor entero del 
	//id del usuario seleccionado en la vista usuarios

	$this->Usuarios->altaAda($id_user, $status, $timecreated, $update_date, $id_role);
	print_r($_POST);	
	redirect('index.php/home/user','refresh');

}


//-------------------------------------------------------------------


	public function eventos(){
		$data = array(
					'modulo' => 'eventos',
					'user_id' => $this->session->id,
					'item_menu' => 'prog_evento'
				);
		
		$this->layout->setLayout('home');
		$this->layout->view('eventos/eventos',$data);	
	}
    
    public function consultasa(){
		$data = array(
			'modulo' => 'eventos',
			'user_id' => $this->session->id,
			'item_menu' => 'seguimineto'
		);


        $this->layout->setLayout('home');
        $this->layout->view('segConsulta',$data);
    }

    public function clasificacion(){
		$data = array( 
			'item_menu' => 'seguimineto'
		);
        $this->layout->setLayout('home');
        $this->layout->view('segClasificacion',$data);
    }
}