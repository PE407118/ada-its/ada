<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sesion extends CI_Controller {
    /*Método para iniciar sesión*/
    public function login(){
        $this->load->helper('api_http_request');
        $url = 'http://172.16.5.85/ada/ada_core/loginuser';
       //$url = 'http://ada.bunam.unam.mx/ada/ada_core/loginuser';

        $user_data = json_decode(postCURL($url, $_POST));
        if ($user_data->status){
            $user_data = $user_data->user_data;
            foreach ($user_data as $clave => $valor) {
                $this->session->{$clave} = $valor;
            }
            redirect(base_url().'index.php/home/ada');
        }else{
            $this->session->error ='Datos de acceso incorrectos';
            redirect();
        }
        
    }
    public function cerrar(){
        $this->session->sess_destroy();
        redirect();
    }
    
}
