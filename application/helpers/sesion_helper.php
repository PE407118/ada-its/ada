<?php
    if (! defined('BASEPATH')) exit('No direct script access allowed');

    function verifica_sesion(){
        if (isset($_SESSION['username'])){
            return true;
        }else if (isset($_SESSION['error'])){
            return $_SESSION['error'];
        }else{
            return false;
        }
    }