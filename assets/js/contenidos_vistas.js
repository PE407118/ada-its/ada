
/*
    *
    * Funciones para generar combo select
    *
*/
    //Funcion a llamar para obtener el combo
    function generaComboSelect(name_select,arrayoptions, key_row_value, key_row_name){
        let combo_select = '<select id="'+name_select+'" class=" form-control" name="'+name_select+'">';
        combo_select +=  optionComboSelect('','Seleccione','selected="selected" disabled="disabled"');
        $.each(arrayoptions, function (indice, option){
            combo_select += optionComboSelect(option[key_row_value], option[key_row_name], '');
        });
        combo_select += '</select>'
        return combo_select;
    }

    //Funcion para generar opción combo select
    function optionComboSelect(value, content, disabled){
        return '<option value="'+value+'" '+disabled+'>'+
                    content+
                    '</option>';
    }

    function generaComboDeshabilitado(){
        return '<select id="curso-evento" class=" form-control" name="curso-evento">'+
                    '<option value="" selected="selected" disabled="disabled">'+
                        'No disponible'+
                    '</option>'+
                '</select>';
    }

/*
    *
    * Definiendo objeto para manejo de tablas que se crean de forma dinámica
    *
*/
    
    class Tabla {
        constructor(clase, id, arreglo_encabezado,arreglo_datos,arreglo_indices ) {
            this.clase = clase;
            this.id = id;
            this.arreglo_encabezado = arreglo_encabezado;
            this.arreglo_datos = arreglo_datos;
            this.arreglo_indices = arreglo_indices;
            this.encabezado = '';
            this.renglones_tabla = '';
        }
        obtenTabla(){
            this.generaEncabezadoTabla();
            this.generaCuerpoTabla();
            return '<div class="table-responsive">'+
                        '<table class="table '+this.clase+'" id="'+this.id+'">'+
                            '<thead>'+
                                this.encabezado +
                            '</thead>'+
                            '<tbody>'+
                                this.renglones_tabla+
                            '</tbody>'+
                        '</table>'+
                    '</div>';
        }
        eliminaTabla(){
            $('#'+this.id).remove();
        }
        generaEncabezadoTabla(){
            let encabezado = '<tr>';
            $.each(this.arreglo_encabezado,function(llave,elemento_encabezado){
                encabezado += '<th>' + elemento_encabezado + '</th>';
            });
            this.encabezado = encabezado+'</tr>';
        }
        generaCuerpoTabla(){
            let renglones_tabla = '';
            let obj_actual = this;
            //console.log(arreglo_datos);
            $.each(this.arreglo_datos,function(llave, arreglo_renglon){
                
                renglones_tabla += obj_actual.generaRenglon(arreglo_renglon);
            });
            this.renglones_tabla = renglones_tabla;
        }

        generaRenglon(arreglo_renglon){
            let renglon = '<tr>';
            $.each(this.arreglo_indices,function(llave,indice_tabla){
                let celda = '';
                if (arreglo_renglon[indice_tabla]!==null){
                    celda = arreglo_renglon[indice_tabla];
                }
                renglon += '<td>'+celda+'</td>';
            });
            renglon += '</tr>';
            return renglon;
        }

    }

