<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/home.css">
    <title>ADA</title>
</head>

<body>
    <!--<div class="container">-->
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
                <a href="<?php echo base_url()?>" class="navbar-brand" id="ada">ADA</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#home-navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="home-navbar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item ">
                            <a href="http://ada.bunam.unam.mx/moodle/ada" target="_blank" class="nav-link">Moodle <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item ">
                            <a href="<?php echo base_url(); ?>index.php/home/user" class="nav-link" id="usuarios" > Usuarios <span class="sr-only"></span></a>
                            <!-- <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url(); ?>index.php/home/user">Consultar</a></li>
                                -<li><a href="<?php echo base_url(); ?>index.php/home/registro">Agregar</a></li>
                            </ul>-->
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/home/eventos" class="nav-link"id="prog_evento" >Programación de Eventos</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" id="seguimineto">
                                Seguimiento Académico </span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="seguimiento">
                                <a href="<?php echo base_url(); ?>index.php/home/consultasa" class="dropdown-item">Consultar</a>
                                <a href="<?php echo base_url(); ?>index.php/home/clasificacion" class="dropdown-item" >Requerimientos</a>
                            </div>
                        </li>
                    </ul>
                    <a href="<?php echo base_url(); ?>index.php/sesion/cerrar" class="navbar-text">Cerrar sesión</a>
                </div>
            </nav>
            
        </header>
        <div class="container-fluid " id="contenido">
            <div class="row justify-content-md-center">
                <div class="col">
                    <?php
                        echo $content_for_layout;
                    ?>
                </div>
            </div>
        </dive>
        <footer >
            <div class="row fixed-bottom bg-dark " id="footer">
                <div class="col-xs-12 col-sm-12 ">
                    <p class="text-center text-white ">
                        Derechos reservados BUNAM
                    </p>
                </div>
            </div>
        </footer>
    <!--</div>-->

    <!-- Funcionamiento de menu -->
    <script>
        item_nav = document.getElementById('<?php echo $item_menu;?>');
        item_nav.classList.add("active");
    </script>
    <!--<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>-->
    
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
    
    
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script src="<?php echo base_url();?>assets/js/clases/ajax.js"></script>


    <script src="<?php echo base_url();?>assets/js/<?php echo $modulo;?>/ajax_<?php echo $modulo;?>.js"></script>
    <script src="<?php echo base_url();?>assets/js/contenidos_vistas.js"></script>
    <script src="<?php echo base_url();?>assets/js/<?php echo $modulo;?>/contenidos_<?php echo $modulo;?>.js"></script>
    <script src="<?php echo base_url();?>assets/js/<?php echo $modulo;?>/class_<?php echo $modulo;?>.js"></script>

    <script src="<?php echo base_url();?>assets/js/<?php echo $modulo;?>/funciones_<?php echo $modulo;?>.js"></script>
    <script src="<?php echo base_url();?>assets/js/<?php echo $modulo;?>/validacion_<?php echo $modulo;?>.js"></script>
    <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>-->


</body>

</html>
