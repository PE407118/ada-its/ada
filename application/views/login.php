<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/estilos.css">
    <title>ADA</title>
</head>

<body>
    <div class="container">
        <div class="col-xs-12 col-sm-12">
            <div class="row alineado">
                <div class="col-xs-12 col-sm-12">
                    <h1>ADA</h1>
                    <p>Apoyo al Desempeño Académico</p>
                    <?php
                        $mensaje_error = ''; 
                        if (!is_null($mensaje)){
                            $mensaje_error = $mensaje;
                        }
                    ?>
<!-- -->
    <div class="container"> 
        <div class="col-xs-12 col-sm-12">
            <div class="row alineado">
                <div class="col-xs-12 col-sm-12">
<!-- Inicio de codigo para formulario de logeo -->
            <section id="login-form-container">
                <form method="post" action="<?php echo base_url();?>index.php/sesion/login">
                    <div class="form-row align-items-center">
                        <div class="col-sm-3 my-1">
                            <input class="form-control form-control-sm" type="text" id="email" name="email" placeholder="ejemplo@dominio.com">
                        </div>
                        <div class="col-sm-3 my-1">
                            <input class="form-control form-control-sm"     type="password" id="password" name="password" placeholder="Password">
                        </div>
                        <div class="col-auto my-1">
                            <input class="btn btn-primary" type="submit" value="Entrar">
                        </div>
                    </div>
                </form>
             </section>
<!-- Fin de codigo para formulario de logeo -->  
        <article class="mensajes">
            <?= $this->load->view('alerts', '',TRUE);?>
        </article>
<!-- -->
        <article class="mensajes_generales" data-mensaje="<?=  $mensaje_error ?>" >
            <?= $this->load->view('alerts', '',TRUE);?>
        </article>
<!-- -->  
                </div>
            </div>
        </div>
    </div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <script src="<?php echo base_url();?>assets/js/<?php echo $modulo;?>/rutas_<?php echo $modulo;?>.js"></script>

    <script src="<?php echo base_url();?>assets/js/clases/ajax.js"></script>
    <script src="<?php echo base_url();?>assets/js/clases/elementosContenedores.js"></script>

    <script src="<?php echo base_url();?>assets/js/contenidos_vistas.js"></script>

    <!--<script src="<?php echo base_url();?>assets/js/<?php echo $modulo;?>/contenidos_<?php echo $modulo;?>.js"></script>-->
    <script src="<?php echo base_url();?>assets/js/<?php echo $modulo;?>/funciones_<?php echo $modulo;?>.js"></script>
    <script src="<?php echo base_url();?>assets/js/<?php echo $modulo;?>/validacion_<?php echo $modulo;?>.js"></script>

    
</body>

</html>