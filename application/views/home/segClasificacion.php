<div class="col-xs-12 col-sm-12">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="alert alert-success" role="alert">
                Especificaciones del curso
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <form>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Curso</label>
                        <select id="curso" class="form-control">
                            <option selected>Choose...</option>
                            <option>Curso 1</option>
                            <option>Curso 2</option>
                            <option>Curso 3</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Grupo</label>
                        <select id="grupo" class="form-control">
                            <option selected>Choose...</option>
                            <option>Grupo 1</option>
                            <option>Grupo 2</option>
                            <option>Grupo 3</option>
                        </select>
                    </div>
                </div>

                <div class="dropdown-divider"></div>

                <div class="row">
                    <div class="col-xs-10 col-sm-12">
                        <form class="form-inline">
                            <h4>Avance Exitoso</h4>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <label class="my-1 mr-2">Ponderación mayor a</label>
                                    <input type="text" class="my-1 mr-sm-1" placeholder="80">
                                    <label class="my-1 mr-2"> de </label>
                                    <input type="text" class="my-1 mr-sm-1" placeholder="100">
                                    <br>
                                </div>
                            </div>
                            <h4>Avance en Riesgo</h4>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <label class="my-1 mr-2">Ponderación </label>
                                    <input type="text" class="my-1 mr-sm-1" placeholder="60">
                                    <label class="my-1 mr-2"> entre </label>
                                    <input type="text" class="my-1 mr-sm-1" placeholder="79">
                                    <label class="my-1 mr-2"> de </label>
                                    <input type="text" class="my-1 mr-sm-1" placeholder="100">
                                    <br>
                                </div>
                            </div>
                            <h4>Avance en Atención</h4>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <label class="my-1 mr-2">Ponderación menor a </label>
                                    <input type="text" class="my-1 mr-sm-1" placeholder="59">
                                    <label class="my-1 mr-2"> de </label>
                                    <input type="text" class="my-1 mr-sm-1" placeholder="100">
                                    <br>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <button type="button" class="btn btn-secondary">Cancelar</button>
                <button type="button" class="btn btn-primary">Guardar</button>
            </form>
        </div>
    </div>
</div>
    