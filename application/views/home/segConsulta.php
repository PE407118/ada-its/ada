<div class="col-xs-12 col-sm-12">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="alert alert-success oculto" role="alert">
                Especificaciones del curso
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <!--form id="form-busqueda">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Curso</label>
                        <select id="curso-evento" class="form-control">
                            <option selected>Choose...</option>
                            <option>Curso 1</option>
                            <option>Curso 2</option>
                            <option>Curso 3</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Grupo</label>
                        <select id="grupo" class="form-control">
                            <option selected>Choose...</option>
                            <option>Grupo 1</option>
                            <option>Grupo 2</option>
                            <option>Grupo 3</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Actividad</label>
                        <select id="actividad" class="form-control">
                            <option selected>Choose...</option>
                            <option>Actividad 1</option>
                            <option>Actividad 2</option>
                            <option>Actividad 3</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Avance</label>
                        <select id="unidad" class="form-control">
                            <option selected>Choose...</option>
                            <option>Todos</option>
                            <option>Exitoso</option>
                            <option>En riesgo</option>
                            <option>En atención</option>
                        </select>
                    </div>
                </div>

                <div class="dropdown-divider"></div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <div class="alert alert-success" role="alert">
                            Información del número total de alumnos inscritos al grupo y su clasificación de avance.
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-10 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">id</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Primer Apellido</th>
                                        <th scope="col">Segundo Apellido</th>
                                        <th scope="col">Avance</th>
                                        <th scope="col">Ponderación</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="bg-success">
                                        <th scope="row">1</th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Exitoso</td>
                                        <td></td>
                                    </tr>
                                    <tr class="bg-warning">
                                        <th scope="row">2</th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Riesgo</td>
                                        <td></td>
                                    </tr>
                                    <tr class="bg-danger">
                                        <th scope="row">3</th>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Atención</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <button type="button" class="btn btn-outline-info">Imprimir</button>
                </div>
            </form-->
            <form id="form-busqueda">
                <div class="form-row">
                    <div class="form-group col-md-6" id="curso-evento">
                        <label>Curso</label>
                        <div class="combo">
                            <select id="curso-evento" class="selectize form-control" name="curso_id">
                                <option value="" selected="selected" disabled="disabled">No disponible</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6" id="grupo-evento">
                        <label>Grupo</label>
                        <div class="combo">
                            <select id="grupo-evento" class="selectize form-control" name="grupo_id">
                                <option value="" selected="selected" disabled="disabled">No disponible</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6" id="unidad-evento">
                        <label>Unidad</label>
                        <div class="combo">
                            <select id="unidad-evento" class="selectize form-control" name="unidad_id">
                                <option value="" selected="selected" disabled="disabled">No disponible</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6" id="actividad-evento">
                        <label>Actividad</label>
                        <div class="combo">
                            <select id="actividad-evento" class="selectize form-control" name="actividad_id">
                                <option value="" selected="selected" disabled="disabled">No disponible</option>
                            </select>
                        </div>
                        <input type="hidden" value="" name="tipo_actividad_id" id="tipo_actividad_id">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

    

