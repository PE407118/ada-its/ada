<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Model {

    public function get_users(){
        return $this->db->query("SELECT mdl_user.id,
                                        mdl_user.username,
                                        mdl_user.firstname,
                                        mdl_user.lastname, 
                                        mdl_role.shortname, 
                                        mdl_user.email, 
                                        mdl_role_assignments.timemodified 
                                    FROM mdl_user,
                                         mdl_role, 
                                         mdl_role_assignments 
                                    WHERE mdl_user.id=mdl_role_assignments.userid 
                                        and mdl_role.id=mdl_role_assignments.roleid
                                ORDER BY mdl_user.id ASC")->result();
    }

    public function registro($id=null){
        return $this->db->query("SELECT mdl_user.id as id_user,
                                        mdl_user.username,
                                        mdl_user.firstname,
                                        mdl_user.lastname,
                                        mdl_role.shortname,  
                                        mdl_role.id as id_role, 
                                        mdl_user.email,
                                        mdl_user.timecreated,
                                        mdl_user.timemodified,
                                        mdl_role_assignments.timemodified 
                                    FROM mdl_user, mdl_role, 
                                        mdl_role_assignments 
                                    WHERE mdl_user.id=mdl_role_assignments.userid 
                                        and mdl_role.id=mdl_role_assignments.roleid
                                        and mdl_user.id=$id
                                ORDER BY mdl_user.id ASC")->result();
    }

    public function altaAda(string $id_user, string $status, string $timecreated, string $update_date, string $id_role){
        return $this->db->query("INSERT INTO user (id, status, create_date, update_date, tipo_user_id) 
                                VALUES ({$id_user}, {$status}, {$timecreated}, {$update_date}, {$id_role})");
    }
}       