<!doctype html>
<html lang="es">
<?php date_default_timezone_set('America/Mexico_City');?>
    <?php
        foreach($masusuarios as $usuarios){
                    $usuarios->id_user;
                    $usuarios->username;
                    $usuarios->firstname;
                    $usuarios->lastname;
                    $usuarios->shortname;
                    $usuarios->email; 
                    date('d/m/Y H:i:s', $usuarios->timemodified);
        }
        echo $update_date = time();
        $status = 1;
    ?>
<br>
<div class="col-xs-12 col-sm-12"> 
    <form method="post" action="<?php echo base_url()?>index.php/Home/altaAda">

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="id">ID</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $usuarios->id_user ?>"  disabled>
            </div>
            <div class="form-group col-md-6">
                <label for="username">Nombre(s)</label>
                <input type="text" class="form-control" id="username" name="username" value="<?php echo $usuarios->username ?>"  disabled>
            </div>
        </div>

        
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="firstname">Primer Apellido</label>
                <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo $usuarios->firstname ?>"  disabled>
            </div>
            <div class="form-group col-md-6">
                <label for="lastname">Segundo Apellido</label>
                <input type="text" class="form-control" id="lastname" name="lastname" value="<?php echo $usuarios->lastname ?>"  disabled> 
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="shortname">Tipo de usuario</label>
                <input type="text" class="form-control" id="shortname" name="shortname" value="<?php echo $usuarios->shortname ?>"  disabled>
            </div>
            <div class="form-group col-md-6">
                <label for="email">email</label>
                <input type="text" class="form-control" id="email" name="email" value="<?php echo $usuarios->email?>"  disabled>
            </div>
        </div>
        <!--Start Hidden-->
        <input type="hidden" id="id" name="id" value="<?php echo $usuarios->id_user ?>">
        <input type="hidden" id="status" name="status" value="<?php echo $status ?>">
        <input type="hidden" id="create_date" name="create_date" value="<?php echo $usuarios->timecreated ?>">
        <input type="hidden" id="update_date" name="update_date" value="<?php echo $update_date ?>">
        <input type="hidden" id="tipo_user_id" name="tipo_user_id" value="<?php echo $usuarios->id_role ?>">
        <!--End hidden-->
        <input type="submit" class="btn btn-primary" value="agregar">
    </form>
</div>


   