-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: ada
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `evento_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `mensaje` longtext NOT NULL,
  `regla_id` int(11) NOT NULL,
  `uapa_id` bigint(10) DEFAULT NULL,
  `user_id` bigint(10) NOT NULL COMMENT 'Usuario que hizo la creación/última modificación del evento',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_Date` timestamp NULL DEFAULT NULL,
  `grupo_id` bigint(10) NOT NULL,
  `actividad_id` bigint(10) DEFAULT NULL COMMENT 'En la base de moodle, este id es el campo id de la tabla correspondiente al name del tipo _actividad_id (module name en moodle)',
  `tipo_actividad_id` bigint(10) DEFAULT NULL COMMENT 'En la base module la tabla se conoce como module',
  `fecha_inicio` timestamp NULL DEFAULT NULL,
  `fecha_fin` timestamp NULL DEFAULT NULL,
  `medio_envio` varchar(45) NOT NULL COMMENT 'Toma el valor de\nAda\nCorreo electrónico\nAmbas\n',
  `porcentaje_ponderacion` float DEFAULT NULL COMMENT 'para la reglas reglas de evaluación con porcentaje satisfactorio/no satisfactorio',
  `curso_id` int(11) DEFAULT NULL,
  `unidad_id` int(11) DEFAULT NULL,
  `paginas_totales_curso` int(11) DEFAULT NULL,
  `fecha_detonacion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`evento_id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;

/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lesson_timer`
--

DROP TABLE IF EXISTS `lesson_timer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lesson_timer` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `lessonid` bigint(10) NOT NULL DEFAULT '0',
  `userid` bigint(10) NOT NULL DEFAULT '0',
  `lessontime_before` bigint(10) NOT NULL DEFAULT '0',
  `lessontime_after` bigint(10) NOT NULL,
  `status_notificacion` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mdl_lesstime_use_ix` (`userid`),
  KEY `mdl_lesstime_les_ix` (`lessonid`)
) ENGINE=InnoDB AUTO_INCREMENT=422 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED COMMENT='Registro del tiempo que un usuario tarda en cambiar de página en una lección ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lesson_timer`
--

LOCK TABLES `lesson_timer` WRITE;
/*!40000 ALTER TABLE `lesson_timer` DISABLE KEYS */;

/*!40000 ALTER TABLE `lesson_timer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mensajes_log`
--

DROP TABLE IF EXISTS `mensajes_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mensajes_log` (
  `mensajes_log_id` int(11) NOT NULL AUTO_INCREMENT,
  `alumno_id` varchar(45) NOT NULL,
  `mensaje_enviado` longtext NOT NULL,
  `medio_envio` varchar(45) NOT NULL,
  `evento_id` bigint(10) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`mensajes_log_id`),
  KEY `evento_id` (`evento_id`),
  CONSTRAINT `mensajes_log_ibfk_1` FOREIGN KEY (`evento_id`) REFERENCES `evento` (`evento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mensajes_log`
--

LOCK TABLES `mensajes_log` WRITE;
/*!40000 ALTER TABLE `mensajes_log` DISABLE KEYS */;

/*!40000 ALTER TABLE `mensajes_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regla`
--

DROP TABLE IF EXISTS `regla`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regla` (
  `regla_id` int(11) NOT NULL AUTO_INCREMENT,
  `regla` varchar(175) NOT NULL,
  `mensaje` longtext,
  `status` int(11) NOT NULL DEFAULT '1',
  `craete_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  `clave` varchar(45) NOT NULL,
  PRIMARY KEY (`regla_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regla`
--

LOCK TABLES `regla` WRITE;
/*!40000 ALTER TABLE `regla` DISABLE KEYS */;
INSERT INTO `regla` VALUES (1,'Actividades en tiempo de la vigencia del grupo','¡Entrega a tiempo tu actividad! Recuerda que la actividad no se ha realizado y el curso está por concluir.',1,'2019-08-26 21:20:59',NULL,'ATG'),(2,'Actividad correspondiente en tiempo específico','¡Entrega a tiempo tu actividad! Recuerda que la actividad tiene una fecha y hora específica de entrega.',1,'2019-08-26 21:20:59',NULL,'ATE'),(3,'Cambiar de página con actividad sin resolver en menos de 60 segundo.','Estimado alumno, le sugerimos revisar el contenido de la siguiente UAPA para complementar sus conocimientos sobre esta habilidad',1,'2019-08-26 21:20:59',NULL,'CAS'),(4,'Ponderación satisfactoria','¡Felicidades lo haz hecho muy bien! Te sugerimos el siguiente material para continuar tu aprendizaje.',1,'2019-08-26 21:20:59',NULL,'PS'),(5,'Ponderación no satisfactoria','¡Puedes hacerlo mejor! Te sugerimos el siguiente material para reforzar tu aprendizaje.',1,'2019-08-26 21:20:59',NULL,'PNS'),(6,'Porcentaje de avance del curso','¡Vamos, tu puedes hacerlo! El curso está por concluir, revisa el porcentaje de avance que tienes en el curso.',1,'2019-08-26 21:20:59',NULL,'PAC'),(7,'Visualización del contenido en un tiempo específico','¡El tiempo sigue avanzando! Procura ir avanzando en el contenido y actividades en el tiempo sugerido.',1,'2019-08-26 21:20:59',NULL,'VCT');
/*!40000 ALTER TABLE `regla` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_user`
--

DROP TABLE IF EXISTS `tipo_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_user` (
  `tipo_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_user` varchar(100) NOT NULL COMMENT 'Recibe valores de \n- Administrador\n- Coordinador\n- Tutor',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tipo_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_user`
--

LOCK TABLES `tipo_user` WRITE;
/*!40000 ALTER TABLE `tipo_user` DISABLE KEYS */;
INSERT INTO `tipo_user` VALUES (1,'Administrador',1,'2019-08-26 21:21:06',NULL),(2,'Coordinador',1,'2019-08-26 21:21:06',NULL),(3,'Tutor',1,'2019-08-26 21:21:06',NULL);
/*!40000 ALTER TABLE `tipo_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uapa`
--

DROP TABLE IF EXISTS `uapa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uapa` (
  `uapa_id` bigint(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uapa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uapa`
--

LOCK TABLES `uapa` WRITE;
/*!40000 ALTER TABLE `uapa` DISABLE KEYS */;
INSERT INTO `uapa` VALUES (1,'uapa1','u1',1,'2019-08-26 21:21:19',NULL),(2,'uapa2','u2',1,'2019-08-26 21:21:19',NULL),(3,'uapa3','u3',1,'2019-08-26 21:21:19',NULL);
/*!40000 ALTER TABLE `uapa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `create_date` bigint(10) NOT NULL,
  `update_date` bigint(10) DEFAULT NULL,
  `tipo_user_id` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (8,1,1574800173,1574800173,3),(7,1,1574800208,1574800208,5),(5,1,1574800212,1574800212,5),(4,1,1574800221,1574800221,5),(3,1,1574800224,1574800224,5),(3,1,1574802389,1574802389,5),(3,1,1574803010,1574803010,5),(4,1,1574803017,1574803017,5);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-26 22:23:04
