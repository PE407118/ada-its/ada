// Funciones para programar los eventos
let user = $('.conts').data('user');
$('.conts').remove();
/***********************************************************************/
/*****VARIABLES DE LOS VALORES DE LOS COMBOS DE MENÚ DE BÚSQUEDA *******/
/***********************************************************************/
let catalogo_cursos = null;
let catalgo_grupos = null;
let catalogo_unidades = null;
let catalogo_actividades = null;
/***************************************************************************************/
/*****VARIABLES DE LOS VALORES DE LOS COMBOS DE FORMULARIO DE REGISTRO DE EVENTO *******/
/***************************************************************************************/
let catalogo_reglas = null;
let catalogo_uapas = null;
let data_evento = null;
/**************************************************************************/
/*****VARIABLES DE LOS VALORES DEL CONTENIDO DE LA TABLA DE EVENTOS *******/
/**************************************************************************/
let eventos_actividad = null;
let eventos_curso = null;

let paginas_totales_curso = null;  

let formulario_eventos = new FormularioEventos();
inicializaPantalla();



/************************************************/
/*****FUNCIONES PARA INICIALIZAR PANTALLA *******/
/************************************************/
function inicializaPantalla(){
    inicializaComboCursos();
    catalogo_reglas = buscarRegla();
    catalogo_uapas = buscarUapa();
    monitoreaComboRegla();

}
/***************************************************************/
/*****FUNCIONES PARA FUNCIONAMIENTO DEL MENÚ DE BÙSQUEDA *******/
/***************************************************************/
function inicializaComboCursos(){
    catalogo_cursos=buscarCursos();
    $('select#curso_id').change(function(){
        let curso_id = $(this).val();
        desactivaComboUnidad();
        estableceEspecificacionesCurso(curso_id);

        inicializaComboGrupos(curso_id);
        
        obtenPaginasTotalesCurso();
    });
}
function obtenPaginasTotalesCurso(){
    let curso_id = $('select#curso_id').val();
    paginas_totales_curso = null;
    paginas_totales_curso = peticion_ada_core.ejecutaGet('cursos/'+curso_id+'/paginas_totales');
    if (paginas_totales_curso){
        if (paginas_totales_curso['status']){
            paginas_totales_curso = paginas_totales_curso['paginas_totales_data']['paginas_totales_curso'];
            $('input#paginas_totales_curso').val(paginas_totales_curso);
            //console.log( paginas_totales_curso);
        }else{
            paginas_totales_curso = null;
            $('input#paginas_totales_curso').val(null);
        }
    }
    //console.log(paginas_totales_curso);
}

function inicializaComboGrupos(curso_id){
    let cambio_curso=true;
    catalogo_grupos = buscarGruposCurso(curso_id);
    $('select#grupo_id').change(function(){
        //Colocando datos del grupo en especificaciones de la actividad
        $('#datos-actividad-seleccionada .dato-actividad#nombre-grupo').html('');
        $('#datos-actividad-seleccionada .dato-actividad#nombre-grupo').append('Nombre del grupo: '+ $(this).children("option:selected").text() );
        $('#datos-actividad-seleccionada .dato-actividad#nombre-grupo').removeClass('oculto');
        desactivaTabaEventos();
        if(cambio_curso){
            inicializaComboUnidades(curso_id);
        }
        cambio_curso=false;
        if (catalogo_actividades!==null){
            formulario_eventos.evento_origen = 2;
            despliegaEventosActividad();
        }
        if (catalogo_unidades!==null){
            if ($('select#unidad_id').val()==='todas'){
                formulario_eventos.evento_origen = 1;
                despliegaEventosCurso();
            }
        }

    });
}

function inicializaComboUnidades(curso_id){
    catalogo_unidades = buscarUnidadesCurso(curso_id);
    //AGREGANDO OPCIÓN TODAS LAS UNIDADES
    let opcion = '<option value="todas">Todas las unidades</option>';
    //$(opcion).insertAfter( $('select#unidad_id').children(":first") );

    $('select#unidad_id').change(function(){
        let unidad_id = $(this).val();
        
        //Agregando nombre del curso en datos de la actividad seleccionada
        $('#datos-actividad-seleccionada .dato-actividad#nombre-actividad').html('');
        $('#datos-actividad-seleccionada .dato-actividad#nombre-unidad').html('');
        $('#datos-actividad-seleccionada .dato-actividad#nombre-unidad').append('Nombre de la unidad: '+ $(this).children("option:selected").text() );
        $('#datos-actividad-seleccionada .dato-actividad#nombre-unidad').removeClass('oculto');
        desactivaComboActividad();

        if ($(this).val() === 'todas'){
            catalogo_actividades=null;
            formulario_eventos.evento_origen = 1;
            despliegaEventosCurso();
        }else{
            inicializaComboActividades(unidad_id);
        }
    });
}
function inicializaComboActividades(unidad_id){
    catalogo_actividades = buscarActividadesUnidad(unidad_id);
    $('select#actividad_id').change(function(){
        let actividad_nombre = $(this).children("option:selected").text()
        $('#datos-actividad-seleccionada .dato-actividad#nombre-actividad').html('');
        $('#datos-actividad-seleccionada .dato-actividad#nombre-actividad').removeClass('oculto');
        $('#datos-actividad-seleccionada .dato-actividad#nombre-actividad').append('Nombre de la actividad: '+ $(this).children("option:selected").text() );
        formulario_eventos.evento_origen = 2;
        console.log("actividad id: "+actividad_nombre)
        estableceInputTipoActividad(actividad_nombre);
        despliegaEventosActividad();
    });
}

/*FUNCIONES DE DESPLIEGUE DE CONTENIDO A PARTIR DEL MENÚ DE BÚSQUEDA*/
function despliegaEventosActividad(){
    eventos_curso=null;
    eventos_actividad=buscarEventosActividad();
    estableceTablaEventosActividad();
    
}
function despliegaEventosCurso(){
    eventos_actividad=null;
    eventos_curso=buscarEventosCurso();
    estableceTablaEventosCurso();
    
}


function estableceEspecificacionesCurso(curso_id){
    let curso = catalogo_cursos.find( curso => curso.id === curso_id );
    let area_contenedora = 'section.especificaciones-curso';

    //Deshabilitando datos de la actividad en el listado de eventos programados para la actividad seleccionada
    $('#datos-actividad-seleccionada .dato-actividad').html('');
    $('#datos-actividad-seleccionada .dato-actividad').addClass('oculto');


    $(area_contenedora).find('#id-curso').html('');
    $(area_contenedora).find('#nombre-curso').html('');
    $(area_contenedora).find('#fecha-inicio-curso').html('');
    $(area_contenedora).find('#fecha-fin-curso').html('');

    $(area_contenedora).find('#id-curso').append(curso['id']);
    $(area_contenedora).find('#nombre-curso').append(curso['fullname'] + ' (' + curso['shortname'] + ') ');
    if (curso['startdate']!=="31-12-1969 06:00:00"){
        $(area_contenedora).find('#fecha-inicio-curso').append(curso['startdate']);
    }else{
        $(area_contenedora).find('#fecha-inicio-curso').append('No establecida');
    }
    if (curso['enddate']!=="31-12-1969 06:00:00"){
        $(area_contenedora).find('#fecha-fin-curso').append(curso['enddate']);
    }else{
        $(area_contenedora).find('#fecha-fin-curso').append('No establecida');
    }
    $(area_contenedora).parent().removeClass('oculto');

    //Agregando nombre del curso en datos de la actividad seleccionada
    $('#datos-actividad-seleccionada .dato-actividad#nombre-curso').html('')
    $('#datos-actividad-seleccionada .dato-actividad#nombre-curso').append('Nombre del curso: '+curso['fullname'] + ' (' + curso['shortname'] + ') ');
    $('#datos-actividad-seleccionada .dato-actividad#nombre-curso').removeClass('oculto');
}

/***************************************************/
/*FUNCIONES PARA EL MANEJO DEL COMBO DE UNIDADES*/
/***************************************************/
function desactivaComboUnidad(){
    deshabilitaCombo('div#unidad-evento');
    desactivaTabaEventos();
    desactivaComboActividad();
    catalogo_unidades = null;
}

/***************************************************/
/*FUNCIONES PARA EL MANEJO DEL COMBO DE ACTIVIDADES*/
/***************************************************/
function desactivaComboActividad(){
    deshabilitaCombo('div#actividad-evento');
    desactivaTabaEventos();
    $('input#tipo_actividad_id').val('');
    $('#datos-actividad-seleccionada').addClass('oculto');
    catalogo_actividades = null;
}
//Función para colocar el input hidden correspondiente al tipo de actividad seleccionado
function estableceInputTipoActividad(actividad_nombre){
    console.log(catalogo_actividades)
    let actividad = catalogo_actividades.find( actividad => actividad.name === actividad_nombre );
    console.log(actividad);
    $('input#tipo_actividad_id').val(actividad['tipo_actividad_id']);
}


/*
    *
    * Funciones para generar tabla de eventos
    *
*/

//Función para desactivar apartado de tablas de eventos
function desactivaTabaEventos(){
    $('section.datos-eventos').addClass('oculto');
    $('section.datos-eventos').find('.tabla-eventos').empty();
    
    $('section.datos-eventos').find('.alert').addClass('oculto');
    $('section.datos-eventos').find('table').closest('.row').addClass('oculto');
    $('section.datos-eventos').find('.alert-danger').html('');
}

function generaTablaEventos(arreglo_datos){
    arreglo_datos = agregaColumnaEditable(arreglo_datos);
    //console.log(arreglo_datos);
    
    let arreglo_indices = ['clave','regla','mensaje','fecha_inicio','fecha_fin','medio_envio','porcentaje_ponderacion','uapa','estado','editar'];
    let arreglo_encabezado = ['Id', 'Regla', 'Mensaje', 'F. Inicio', 'F. Fin','Medio de notificación','Ponderación','UAPA','Estado',''];
    let tabla_eventos = new Tabla('tabla-eventos','eventos-actividad',arreglo_encabezado,arreglo_datos,arreglo_indices);
    return tabla_eventos.obtenTabla();
}
function agregaColumnaEditable(arreglo_datos){
    $.each(arreglo_datos, function (indice, renglon_actual){
        if(renglon_actual['status']==1){
            renglon_actual['estado']='Activo';
        }else{
            renglon_actual['estado']='Inactivo';
        }
        indice_renglon = indice +1;
        renglon_actual['editar'] = '<button type="button" class="btn btn-primary disparador-modal editar-evento" data-numero-renglon="'+indice_renglon+'" data-toggle="modal" data-target="#modal-form-eventos">'+
                                        'Editar evento'+
                                    '</button>';
    });
    return arreglo_datos;
}

//Función para colocar el arreglo de actividades obtenido en la tabla de datos de eventos de actividad de la actividad y grupo seleccionados
function estableceTablaEventosActividad(){
    let selector_seccion_tabla = '#datos-actividad-seleccionada';
    desactivaTabaEventos();
    $('#boton-agregar-evento').removeClass('oculto');
    $('#datos-actividad-seleccionada').removeClass('oculto');
    if(eventos_actividad){
        if (eventos_actividad['status']){
            //MOSTRANDO TABLA DE EVENTOS DE LA ACTIVIDAD
            $(selector_seccion_tabla).find('.alert-success').removeClass('oculto');
            let tabla_eventos_cad = generaTablaEventos(eventos_actividad['eventos_data']);
            $(selector_seccion_tabla).find('.tabla-eventos').append(tabla_eventos_cad);
        }else{
            $(selector_seccion_tabla).find('.alert-danger').append(eventos_actividad['message'])
            $(selector_seccion_tabla).find('.alert-danger').removeClass('oculto');
        }
    }else{
        $(selector_seccion_tabla).find('.alert-warning').removeClass('oculto');
    }
    recargaFuncionBotonEditarEvento();
}

//Función para colocar el arreglo de actividades obtenido en la tabla de datos de eventos de actividad de la actividad y grupo seleccionados
function estableceTablaEventosCurso(){
    let selector_seccion_tabla = '#datos-curso-seleccionado';
    desactivaTabaEventos();
    $('#boton-agregar-evento').removeClass('oculto');
    $(selector_seccion_tabla).removeClass('oculto');
    if(eventos_curso){
        if (eventos_curso['status']){
            //console.log(eventos_curso['eventos_data']);
            let existe_tabla_sin_actividad = obtenTablaEventosCursoSinActividad();
            let existe_tabla_con_actividad = obtenTablasEventosCursoConActividad();
            if (!existe_tabla_con_actividad && !existe_tabla_sin_actividad){
                $(selector_seccion_tabla).find('.alert-danger').append('<p>No existen actividades programadas para este curso</p>')
                $(selector_seccion_tabla).find('.alert-danger').removeClass('oculto');
            }
        }else{
            $(selector_seccion_tabla).find('.alert-danger').append(eventos_actividad['message'])
            $(selector_seccion_tabla).find('.alert-danger').removeClass('oculto');
        }
    }else{
        $(selector_seccion_tabla).find('.alert-warning').removeClass('oculto');
    }
    recargaFuncionBotonEditarEvento();
}

function obtenTablaEventosCursoSinActividad(){
    if (eventos_curso['eventos_data']['sin-actividad']){
        let nombre_curso = $('select#curso_id').children("option:selected").html();
        let nombre_grupo = $('select#grupo_id').children("option:selected").html();
        //alert (nombre_grupo);
        let tabla_eventos = generaTablaEventos(eventos_curso['eventos_data']['sin-actividad']);
        let panel_eventos = generaColapsableEventos('sin-actividad', nombre_curso, nombre_grupo, false, false, false, tabla_eventos);
        
        $('#datos-curso-seleccionado section.tabla-eventos').append(panel_eventos);
        $('#datos-curso-seleccionado').removeClass('oculto');

        return true;
    }
    return false;
}
function obtenTablasEventosCursoConActividad(){
    //console.log(eventos_curso);
    let respuesta = false;
    if (eventos_curso['eventos_data']['con-unidad-actividad']){
        //console.log('con actividad');
        //console.log(eventos_curso['eventos_data']['con-unidad-actividad']);
        $.each(eventos_curso['eventos_data']['con-unidad-actividad'], function (llave, unidad){
            let unidad_id = unidad['id'];
            let unidad_nombre = unidad['name'];
            $.each(unidad['actividades'] , function (llave2, actividad){
                let actividad_id = actividad['id'];
                let actividad_nombre = actividad ['name'];
                let tipo_actividad_id = actividad ['tipo_actividad_id'];
                let tabla_eventos = generaTablaEventos(actividad['eventos']);
                let panel_eventos = generaColapsableEventos('actividad-'+actividad_id, unidad_nombre, actividad_nombre, unidad_id, actividad_id, tipo_actividad_id, tabla_eventos);
                $('#datos-curso-seleccionado section.tabla-eventos').append(panel_eventos);
                $('#datos-curso-seleccionado').removeClass('oculto');
                respuesta = true;
            });
        });

    }
    return respuesta;
}


/************************************************/
/*FUNCIONES PARA EL MANEJO GENERAL DE LOS COMBOS*/
/************************************************/
//Función para agregar combo select en formulario de búsqueda
function agregaComboSelect(identificador_seccion, combo){
    $(identificador_seccion + ' .combo').html('');
    $(identificador_seccion + ' .combo').append(combo);
}
//Función para deshabilitar combo(identificador_seccion)
function deshabilitaCombo(identificador_seccion){
    combo = generaComboDeshabilitado();
    agregaComboSelect(identificador_seccion, combo);
}



/**************************************************/
/*FUNCIONES PARA OBTENER COMBOS SELECT DE BÚSQUEDA*/
/**************************************************/
// Función para cargar el combo de cursos
function buscarCursos(){
    //console.log('cargando script');
    let combo_curso = getCatalogo('cursos');
    //console.log(combo_curso);
    let identificador_seccion='div#curso-evento';
    if (combo_curso){
        if (combo_curso['status']==true){
            let catalogo_cursos = generaComboSelect('curso_id',combo_curso['cursos_data'], 'id', 'fullname');
            agregaComboSelect(identificador_seccion, catalogo_cursos);
            return combo_curso['cursos_data'];
        }
    }
    deshabilitaCombo(identificador_seccion);
    return false;
}

// Función para cargar el combo de unidades de un curso
function buscarUnidadesCurso(curso_id){
    let combo_unidad = getCatalogoReferenciado('cursos','unidades',curso_id);
    let identificador_seccion = 'div#unidad-evento';
    if (combo_unidad){
        if (combo_unidad['status']==true){
            let catalogo_unidades = generaComboSelect('unidad_id',combo_unidad['unidades_data'], 'id', 'name');
            agregaComboSelect(identificador_seccion, catalogo_unidades);
            return combo_unidad['unidades_data'];
        }
    }
    deshabilitaCombo(identificador_seccion);
    return false;
}

function buscarGruposCurso(curso_id){
    let combo_grupo = getCatalogoReferenciado('cursos','grupos',curso_id);
    let identificador_seccion = 'div#grupo-evento';
    if (combo_grupo){
        if (combo_grupo['status']==true){
            let catalogo_grupos = generaComboSelect('grupo_id',combo_grupo['grupos_data'], 'id', 'name');
            agregaComboSelect(identificador_seccion, catalogo_grupos);
            return combo_grupo['grupos_data'];
        }
    }
    deshabilitaCombo(identificador_seccion);
    return false;
}

function buscarActividadesUnidad(unidad_id){
    let combo_actividad = getCatalogoReferenciado('unidades','actividades',unidad_id);
    let identificador_seccion = 'div#actividad-evento';
    if (combo_actividad){
        if (combo_actividad['status']==true){
            let catalogo_actividades = generaComboSelect('actividad_id',combo_actividad['actividades_data'], 'id', 'name');
            agregaComboSelect(identificador_seccion, catalogo_actividades);
            return combo_actividad['actividades_data'];
        }
    }
    deshabilitaCombo(identificador_seccion);
    return false;
}

/*****************************************************/
/*FUNCIONES PARA OBTENER COMBOS SELECT DE FORM EVENTO*/
/*****************************************************/
function buscarRegla(){
    let combo_regla =  getCatalogoReglas();
    let identificador_seccion = 'div#reglas-evento';
    if(combo_regla){
        if(combo_regla['status'] == true){
            let catalogo_reglas = generaComboSelect('regla_id', combo_regla['regla_data'], 'regla_id', 'regla');
            agregaComboSelect(identificador_seccion, catalogo_reglas);
            return combo_regla['regla_data'];
        }
    }
    deshabilitaCombo(identificador_seccion);
    return false;
}


function buscarUapa(){
    let combo_uapa =getCatalogo('uapas');
    let identificador_seccion = 'div#uapas-evento';
    if(combo_uapa){
        if(combo_uapa['status']==true){
            let catalogo_uapas = generaComboSelect('uapa_id', combo_uapa['uapa_data'], 'uapa_id', 'nombre');
            agregaComboSelect(identificador_seccion, catalogo_uapas)
            return combo_uapa['uapa_data'];
        }
    }
    deshabilitaCombo(identificador_seccion);
    return false;
}

/**********************************************/
/*FUNCIONES PARA OBTENER LAS TABLAS DE EVENTOS*/
/**********************************************/

//Función que obtiene los eventos programados para una actividad y grupo específico
function buscarEventosActividad(){
    //Obteniendo los parámetros de búsqueda para el evento: grupo, módulo (tipo_actividad), instance (actividad_id)
    let grupo_id            = $('select#grupo_id').val();
    let tipo_actividad_id   = $('input#tipo_actividad_id').val();
    let actividad_id        = $('select#actividad_id').val();
    //Consultando los eventos dentro de la base de datos
    let eventos_actividad = getEventosActividad(grupo_id,tipo_actividad_id,actividad_id);
    
    if (eventos_actividad){       
        return eventos_actividad;
    }
    return false;
}

//Función que obtiene los eventos programados para un curso y grupo específico
function buscarEventosCurso(){
    //Obteniendo los parámetros de búsqueda para el evento: grupo, módulo (tipo_actividad), instance (actividad_id)
    let grupo_id            = $('select#grupo_id').val();
    let curso_id   = $('select#curso_id').val();
    
    //Consultando los eventos dentro de la base de datos
    let eventos_curso = getEventosCurso(grupo_id,curso_id);
    if (eventos_curso){       
        return eventos_curso;
    }
    return false;
}



/**
 * 
 * PARA EL FORMULARIO DE ENVÍO 
 * 
*/

// Se valida las opciones de CURSO / GRUPO / TODAS LAS UNIDADES para desplegar las reglas correspondientes.
        //  OPCIONAL
// Se valida si la regla implica ponderación se deben mostrar el campos de ponderación y uapa.
function monitoreaComboRegla(){
    $('#mensaje').val('');
    $('select#regla_id').change(function(){
        //console.log($(this).val());
        //habilitando combo de uapas
        $('div#paginas_totales_curso').addClass('oculto');
        if($(this).val()== 4 || $(this).val() == 5){
            $('.ponderacion').removeClass('oculto');
            $('.combo-uapas').removeClass('oculto');

       
                // Con las opciones anteriores se despliega el catálogo de uapas
            //let select_uapas = generaComboSelect('uapa_id', catalogo_uapas, 'uapa_id', 'nombre');
            //agregaComboSelect('div#uapas-evento', select_uapas)
        } else {
            //deshabilitaCombo('div#uapas-evento'); // Se deshabilita el combo para que no cargue ninguna uapa aun en el cambio de regla.
            $('input#porcentaje_ponderacion').val('');
            $('.ponderacion').addClass('oculto');
            $('.combo-uapas').addClass('oculto');
            if ($(this).val()==6){
                $('input#paginas_totales_curso').val(paginas_totales_curso);
                $('div#paginas_totales_curso').removeClass('oculto');
            }
        }
        if($(this).val()== 1 || $(this).val() == 2){
            $('div#tiempo_antes').removeClass('oculto');
        }else{
            $('div#tiempo_antes').addClass('oculto');
        }
        alertaVigenciaGrupo();

        //asignando valor del mensaje de la regla seleccionada
        let regla = catalogo_reglas.find(regla => regla.regla_id === $(this).val());
       
        $('#mensaje').val(regla['mensaje']);
    });
}


 $('#evento-form').submit(function(event){
    reiniciarAlertasModalEvento();
    let camposFormBusqueda = null;
    let camposEventoForm = null;

    

    // Se obtienen los valores de los campos del #form-busqueda
    camposFormBusqueda = $('#form-busqueda').serializeArray().reduce(function(obj, item){
        obj[item.name] = item.value;
       if(item.value === ''){
            obj[item.name] = undefined;
        } 
        
        return obj;
    }, {});

    // Se obtienen los valores de los campos del ##evento-form
    camposEventoForm = $('#evento-form').serializeArray().reduce(function(obj,item){
        obj[item.name] = item.value;
        if(item.value === ''){
            obj[item.name] = undefined;
        }
        return obj;
    }, {});
    
    // Se unen los dos objetos de cada formulario en uno solo(data_evento)
    data_evento = $.extend(camposFormBusqueda,camposEventoForm);
    data_evento['user_id']= user;
    console.log( data_evento );
    event.preventDefault();
    
    //console.log(data_evento);
    //tipo_accion = 1  => Se desea agregar
    //tipo_accion = 2  => Se desea actualizar
    if (formulario_eventos.tipo_accion === 1){
        respuesta_evento = postEnviarRegla(data_evento);
    }else{
        respuesta_evento = putEnviarRegla(data_evento);
    }
    
    if (eventos_actividad ===null){
        despliegaEventosCurso();
        $('.collapse').collapse();

    }else if (eventos_curso ===null){
        despliegaEventosActividad();
    }
    console.log(respuesta_evento);
    if(respuesta_evento === false){//Se muestra este mensaje en amarillo cuando no existe la conexión con el servidor de core
        $('.mi-warning').html('<p>No se guadardo la programación del evento.</p>' + '<p>Intente mas tarde o consulte con el administrador del sitio.</p>');
        $('.mi-warning').removeClass('oculto');
        
    } else if(respuesta_evento['status']==false){ //Se muestra este mensaje en rojo cuando no se llenan los campos correspondientes
        $('.mi-danger').html(respuesta_evento['message']);
        $('.mi-danger').removeClass('oculto');
        
        } else { //Se muestra este mensaje en verde cuando se almaceno exitosamente en el core
            
            $('.mi-success').removeClass('oculto');
            $('.mi-success').html(respuesta_evento['message']);
            obtenPaginasTotalesCurso();
            $('div#paginas_totales_curso').addClass('oculto');
            if (formulario_eventos.tipo_accion === 1){
                $('#evento-form')[0].reset();
                
            }
        }
 });




/*** FUNCIÓN DE DATEPIKER PARA LOS EVENTOS (FECHAS DE INICIO Y FIN) */

$(function() {
    $('#inicio_fecha').datetimepicker({
        format: "DD/MM/YYYY hh:mm:ss a"
    });
    $('#fin_fecha').datetimepicker({
        format: "DD/MM/YYYY hh:mm:ss a"
    });
  });




/*CONTROL DE DESPLIEGUE DE FORMULARIO PARA AGREGAR/ACTUALIZAR EVENTOS*/

function reiniciarAlertasModalEvento(){
    $('#advertencia-paginas-totales').addClass('oculto');
    $('#modal-form-eventos .alert').addClass('oculto');
    $('.mi-danger').html('');
    $('.mi-success').html('');
}

$('button.agregar-evento').click(function(){
    obtenPaginasTotalesCurso()
    reiniciarAlertasModalEvento();
    $('#evento-form')[0].reset();
    formulario_eventos.tipo_accion = 1;
    formulario_eventos.cargaFormulario();
    $("#modal-form-eventos").modal();
});
function recargaFuncionBotonEditarEvento(){
    //alert ('recargando')
    
    $('button.editar-evento').unbind();
    $('button.editar-evento').click(function(){
        reiniciarAlertasModalEvento();
        //alert ('recuperacion de funciones botón')
        formulario_eventos.tipo_accion = 2;
        formulario_eventos.boton_activacion = $(this);
        formulario_eventos.cargaFormulario();
        alertaVigenciaGrupo();
    });    
}

$('button.cambia-status-evento').click(function(){
    obtenPaginasTotalesCurso();
    reiniciarAlertasModalEvento();
    let evento_id = $('#modal-form-eventos input#evento_id').val();
    let status = $('#modal-form-eventos input#status').val();
    let cambio_status = putCambiaEstadoEvento(evento_id, status);
    if (cambio_status){
        let selector = '.mi-danger';
        if (cambio_status['status']){
            selector = '.mi-success';
            if (status == 1){
                $('#modal-form-eventos input#status').val(0);
                $('button.cambia-status-evento').html('Desactivar');
            }else{
                $('#modal-form-eventos input#status').val(1);
                $('button.cambia-status-evento').html('Activar');
    
            }
            if (eventos_actividad ===null){
                despliegaEventosCurso();
                $('.collapse').collapse()

            }else if (eventos_curso ===null){
                despliegaEventosActividad();
            }
        }
        $(selector ).html(cambio_status['message']);
        $(selector).removeClass('oculto');
        
    }else{
        $('.mi-warning').html('<p>No se pudo actualizar el estado del evento.</p>' + '<p>Intente mas tarde o consulte con el administrador del sitio.</p>');
        $('.mi-warning').removeClass('oculto');
    }
});

//MONITOREANDO VIGENCIA DEL GRUPO

    
//$('input.fechas-vigncia-grupo-evento').click(despliegaAdvertenciaVigenciaGrupo());

function despliegaAdvertenciaVigenciaGrupo(){

        $('#advertencia-vigencia-grupo').removeClass('oculto');
        $('#advertencia-vigencia-grupo .alert').removeClass('oculto');
}

function alertaVigenciaGrupo(){
    //Para la vigencia del grupo
    if($('select#regla_id').val()== 1 || $('select#regla_id').val() == 3 || $('select#regla_id').val() == 4|| $('select#regla_id').val() == 5){
        $('.fecha-evento').addClass('fechas-vigncia-grupo-evento');
        despliegaAdvertenciaVigenciaGrupo();
    }else{
        $('.fecha-evento').removeClass('fechas-vigncia-grupo-evento');
        $('#advertencia-vigencia-grupo').addClass('oculto');
        $('#advertencia-vigencia-grupo .alert').addClass('oculto');
    }
}