<form id="form-busqueda">
    <div class="form-row">
        <div class="form-group col-md-6" id="curso-evento">
            <label>Curso</label>
            <div class="combo">
                <select id="curso-evento" class=" form-control" name="curso_id">
                    <option value="" selected="selected" disabled="disabled">No disponible</option>
                </select>
            </div>
        </div>
        <div class="form-group col-md-6" id="grupo-evento">
            <label>Grupo</label>
            <div class="combo">
                <select id="grupo-evento" class=" form-control" name="grupo_id">
                    <option value="" selected="selected" disabled="disabled">No disponible</option>
                </select>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6" id="unidad-evento">
            <label>Unidad</label>
            <div class="combo">
                <select id="unidad-evento" class=" form-control" name="unidad_id">
                    <option value="" selected="selected" disabled="disabled">No disponible</option>
                </select>
            </div>
        </div>
        <div class="form-group col-md-6" id="actividad-evento">
            <label>Actividad</label>
            <div class="combo">
                <select id="actividad-evento" class=" form-control" name="actividad_id">
                    <option value="" selected="selected" disabled="disabled">No disponible</option>
                </select>
            </div>
            <input type="hidden" value="" name="tipo_actividad_id" id="tipo_actividad_id">
        </div>
    </div>
</form>