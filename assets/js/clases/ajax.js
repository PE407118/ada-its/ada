class PeticionAjax {
    constructor( uname, password, host, async, dataType) {
        this.uname = uname;
        this.password = password;
        this.host = host;
        this.async = async;
        this.dataType = dataType
    }
    ejecutaGet(url){
        let respuesta = false;
        $.ajax({
            method: 'GET',
            url: this.host + url,
            async: this.async,
            dataType: this.dataType,
            username: this.uname,
            password: this.password
        }).done(function(response){
            if(response){
                respuesta = response;
                
            }
        }).fail(function(jqXHR, textStatus){
            //console.log (jqXHR);
        });
        //console.log (respuesta);
        return respuesta;
    }
    ejecutaPost(url,data){
        mandaPeticionPostPut('POST', url, data);
    }
    ejecutaPut(url,data){
        mandaPeticionPostPut('PUT', url, data);
    }
    mandaPeticionPostPut(metodo, url, data){
        let respuesta = false;
        $.ajax({
            method: metodo,
            url: this.host + url,
            async: this.async,
            dataType: this.dataType,
            data: data,
            username: this.uname,
            password: this.password
        }).done(function(response){
            if(response){
                respuesta = response;
                
            }
        }).fail(function(jqXHR, textStatus){
            console.log (jqXHR);
        });
        return respuesta;
    }

}
let peticion_ada_core = new PeticionAjax("ada-site","gztuZhdBj4kaYf4q","http://localhost/ada/ada_core/", false,'json');
//let peticion_ada_core = new PeticionAjax("ada-site","gztuZhdBj4kaYf4q","http://ada.bunam.unam.mx/ada/ada_core/", false,'json');
