<div class="col-xs-12 col-sm-12">
    <div class="row">
        <div class="col-xs-10 col-sm-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <?php date_default_timezone_set('America/Mexico_City');?>
                    <thead>
                        <tr>
                            <th scope="col">id</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Primer Apellido</th>
                            <th scope="col">Segundo Apellido</th>
                            <th scope="col">Tipo Usuario</th>
                            <th scope="col">Correo</th>
                            <th scope="col">Fecha de Registro</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <?php
                            foreach($losusuarios as $usuarios){
                                echo
                                "<tr>" .
                                    "<td>" . 
                                        $usuarios->id .
                                    "</td>";
                                echo
                                    "<td>" . 
                                        $usuarios->username .
                                    "</td>";
                                echo
                                    "<td>" . 
                                        $usuarios->firstname .
                                    "</td>";
                                echo
                                    "<td>" . 
                                        $usuarios->lastname .
                                    "</td>";
                                echo
                                    "<td>" . 
                                        $usuarios->shortname .
                                    "</td>";
                                echo
                                    "<td>" . 
                                        $usuarios->email .
                                    "</td>";
                                echo
                                    "<td>" . 
                                        date('d/m/Y H:i:s', $usuarios->timemodified ) .
                                    "</td>";

                                    echo
                                    "<td>" .
                                        "<a href=" . base_url() . "index.php/home/registro/"
                                        . $id = $usuarios->id
                                        . ">" . "+</a>".
                                    "</td>" .
                                "</tr>";
                            }
                        ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


    