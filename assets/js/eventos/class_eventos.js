class FormularioEventos {
    //evento_origen = 1  => Se dispara desde todas las unidades
    //evento_origen = 2  => Se dispara desde seleccionar una actividad en especial
    evento_origen=null;

    //tipo_accion = 1  => Se desea agregar
    //tipo_accion = 2  => Se desea actualizar
    tipo_accion = null;
    
    cargaFormulario(){
        $('select#regla_id option').removeAttr("disabled");
        $('select#regla_id').val(null);
        $('select#regla_id option').first().attr("disabled", "disabled");

        $('input:radio[name=medio_envio]').prop("checked", false);
        this.cargaTipoFormulario();
        this.cambiaEstadoPantalla();
        
    }
 
    cargaTipoFormulario(){
        if (this.tipo_accion == 1){
            
            this.reiniciaComboUapas();
            $('input#evento_id').val('');
            $('button.cambia-status-evento').addClass('oculto');
            $('button.enviar-info-evento').html('Agregar');
            //peticion_ada_core.ejecutaGet('grupo/'+$('select#grupo_id').val()+'/vigencia');
        }else{
            $('button.cambia-status-evento').removeClass('oculto');
            $('button.enviar-info-evento').html('Actualizar');
            this.recuperaDatosOriginales();
        }
    }

    reiniciaComboUapas(){
        $('div#uapas-evento .combo select option').removeAttr("disabled");
        $('div#uapas-evento .combo select').val(null);
        $('div#uapas-evento .combo select option').first().attr("disabled", "disabled");
    }

    recuperaDatosOriginales(){
        //RECUPERANDO DATOS DE UNIDAD Y ACTIVIDAD
        let renglon = this.boton_activacion.closest("tr");
        let acordion = this.boton_activacion.closest('.accordion');
        this.unidad_id = acordion.data('unidad');
        this.actividad_id = acordion.data('actividad');
        if (this.unidad_id !== false && this.actividad_id !== false){
            this.evento_origen = 2;
        }
        //RECUPERANDO DATOS DE TABLA EVENTO
        
        //Recuperando identificador de evento a modificar
        let clave_evento =renglon.children(':nth-child(1)').html();
        
        $('input#evento_id').val(clave_evento.match(/[0-9]+$/g));

        // Estableciendo combo de regla
        this.buscaIdOpcionDada(renglon.children(':nth-child(2)').html(), 'select#regla_id');
        // Estableciendo mensaje de notificación del evento
        $('#mensaje_regla #mensaje').val(renglon.children(':nth-child(3)').html());
        //Estableciendo fechas inicio/fin
        //$('#inicio_fecha').data("DateTimePicker").date(renglon.children(':nth-child(4)').html());

        $('#fin_fecha').data("DateTimePicker").date(renglon.children(':nth-child(5)').html());

        //$('#inicio_fecha input').trigger('change');
        //$('#fin_fecha input').trigger('change');
        //Estableciendo opcion de medio de envío        
        this.buscaOpcionRadioDada(renglon.children(':nth-child(6)').html(),'input:radio[name=medio_envio]');
        //Estableciendo datos de la ponderación
        $('#ponderacion input').val(renglon.children(':nth-child(7)').html());
        // Estableciendo combo de uapa
        //console.log(renglon.children(':nth-child(8)').html());
        if (renglon.children(':nth-child(9)').html() == 'Activo'){
            let status = $('#modal-form-eventos input#status').val(0);
            $('button.cambia-status-evento').html('Desactivar');
        }else{
            let status = $('#modal-form-eventos input#status').val(1);
            $('button.cambia-status-evento').html('Activar');
        }
        

        if (renglon.children(':nth-child(8)').html() !== ''){
            this.buscaIdOpcionDada(renglon.children(':nth-child(8)').html(), 'div#uapas-evento .combo select');
        }else{
            this.reiniciaComboUapas();
        }
    }
    buscaOpcionRadioDada(valor_dado, selector_radio){
        $(selector_radio).each(function(){
            
            if ($(this).val()== valor_dado){
                $(this).prop("checked", true);
                
            }else{
                $(this).prop("checked", false);
            }
        });
    }

    buscaIdOpcionDada(texto_opcion, selector_combo){
        //console.log(selector_combo)
        $(selector_combo + ' option').each(function(){
            //console.log($(this).html());
            if ($(this).html() == texto_opcion){
                $(selector_combo).val($(this).val());
                $(selector_combo).trigger('change');
            }
        });
    }
    
    cambiaEstadoPantalla(){
        $('select#regla_id option').removeAttr("disabled");
        if(this.evento_origen == 1){
            this.activaOpcionesGeneral();
        }else{
            this.activaOpcionesActividad();
        }
    }

    //Opciones para agregar/modificar reglas que no estén ligadas a una unidad ni actividad
    //evento_origen =1
    activaOpcionesGeneral(){
        $('select#regla_id option').each(function (){
            if($(this).val()!= 6 && $(this).val()!= 7){
                $(this).attr("disabled", true);
            }
        });
    }
    
    //Opciones para agregar/modificar reglas que están ligadas a una actividad de uniad específica
    //evento_origen =1
    activaOpcionesActividad(){
        $('select#regla_id option').each(function (){
    
            let tipo_actividad = $('#tipo_actividad_id').val();
            console.log(tipo_actividad);
            console.log($(this).val());
            if(($(this).val()== 6 || $(this).val()== 7)||
            ($(this).val()==3 && tipo_actividad != 13)||
            ($(this).val()<3 && !(tipo_actividad == 16 || tipo_actividad ==1 || tipo_actividad ==9 ))  
            ){
                $(this).attr("disabled", true);
            }else{
                $(this).removeAttr("disabled")
            }
        });
        this.cambiaMenuBusqueda();
    }
    cambiaMenuBusqueda(){
        //Verificando si es necesario cambiar menú de búsqueda
        let comboUnidad = $('#unidad-evento .combo select');
        if (comboUnidad.val()=='todas'){
            //Simulando la selección de las opciones de unidad y actividad correspondientes
            comboUnidad.val(this.unidad_id);
            comboUnidad.trigger('change');
            let comboActividad = $('#actividad-evento .combo select');
            comboActividad.val(this.actividad_id);
            comboActividad.trigger('change');
            recargaFuncionBotonEditarEvento();
        }
    }

}
