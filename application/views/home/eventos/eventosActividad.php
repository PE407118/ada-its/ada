
<section class="oculto datos-eventos" id="datos-actividad-seleccionada">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="alert alert-success" role="alert">
                <p class="dato-actividad oculto" id='nombre-curso'>Nombre del curso</p>
                <p class="dato-actividad oculto" id='nombre-grupo'>Nombre del grupo</p>
                <p class="dato-actividad oculto" id='nombre-unidad'>Nombre de la unidad</p>
                <p class="dato-actividad oculto" id='nombre-actividad'>Nombre de la actividad</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="alert alert-warning" role="alert">
                <p>Sistema de consulta de eventos temporalmente fuera de servicio.</p>
                <p>Intente mas tarde o consulte con el administrador del sitio.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="alert alert-danger" role="alert">
                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 tabla-eventos">
            
        </div>
    </div>
</section>