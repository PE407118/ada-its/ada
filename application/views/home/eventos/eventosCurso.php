<section class="oculto datos-eventos" id="datos-curso-seleccionado">
    
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="alert alert-warning" role="alert">
                <p>Sistema de consulta de eventos temporalmente fuera de servicio.</p>
                <p>Intente mas tarde o consulte con el administrador del sitio.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="alert alert-danger" role="alert">
                
            </div>
        </div>
    </div>
    <div class="row">
        <section class="col-xs-12 col-sm-12">
            <section class="tabla-eventos"></section>
        </section>
    </div>
</section>