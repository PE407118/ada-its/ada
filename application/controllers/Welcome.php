<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function _index()
	{
		redirect(base_url().'index.php/home/ada');
	}

	public function index()
	{
		$sesion_existente = verifica_sesion();
		$mensaje = null;
		if ($sesion_existente){
			if ($sesion_existente === true){
				redirect('index.php/home/ada');
			}
			$mensaje = $sesion_existente;
		}
		$this->session->sess_destroy();
		$data = array(
					'modulo'  => 'login', 
					'mensaje' => $mensaje
				);		
		$this->load->view('login',$data);
		
	}
	
}
