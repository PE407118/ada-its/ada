class Combo{
    /*
        Estructura del array elementos_desactivar
        array (
            'sin_valor' => array(
                ob1, ob2
            )
            'con_valor => array(
                'v_select' => array(
                    ob1, ob2
                ),
                v2_select' => array(
                    ob3, ob4
                )
            )
        )
    */

    constructor( id_seccion_contenedora, id_combo, name_combo,elementos_desactivar) {
        this.identificador_seccion_contenedora = id_seccion_contenedora;
        this.id_combo = id_combo;
        this.name_combo = name_combo;
        this.elementos_desactivar = this.elementos_desactivar;
    }

    buscarCombo(key_data, key_value_select, key_value_option){
        deshabilita(identificador_seccion);
        let combo_data = peticion_ada_core.ejecutaGet(this.url);
        
        if (combo_data){
            if (combo_data==true){
                this.combo_select = generaComboSelect(this.id_combo,combo_data[key_data], key_value_select, key_value_option);
                this.agregaComboSelect();
                this.combo_data=combo_data[key_data];
            }
        }
        
    }
    agregaComboSelect(){
        $(this.id_seccion_contenedora + ' .combo').html('');
        $(this.id_seccion_contenedora + ' .combo').append(this.combo_select);
    }
    deshabilita(){
        this.combo_data=false;
        this.combo_select = generaComboDeshabilitado();
        agregaComboSelect();
        this.deshabilitaDependientes();
    }
    deshabilitaDependientes(){
        let combo = $this;
        elementos_desactivar.each(function(elemento_tipo){
            
        })
    }
}