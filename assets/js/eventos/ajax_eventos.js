var uname="ada-site";
var password="gztuZhdBj4kaYf4q";
var host = "http://172.16.5.85/ada/ada_core/";
//var host = "http://ada.bunam.unam.mx/ada/ada_core/";
/*
    *
    *  FUNCIONES AJAX PARA GET
    *
*/
    // Función para obtener el contenido completo de la ruta dada
    function getCatalogo(ruta_catalogo){
        let catalogo = false;
        $.ajax({
            method: 'GET',
            url: host + ruta_catalogo,
            async: false,
            dataType: 'json',
            username: uname,
            password: password
        }).done(function(response){
            console.log(response);
            if(response){
                catalogo = response;
                
            }
        }).fail(function(jqXHR, textStatus){
            console.log(jqXHR);
        });
        return catalogo;
    }

    // Función para obtener el contenido de una tabla dado el id de otra
    function getCatalogoReferenciado(tabla_referenciada,tabla_objetivo,id_tabla_referenciada){
        let catalogo = false;
        $.ajax({
            method: 'GET',
            url: host + tabla_referenciada + '/' + id_tabla_referenciada + '/' + tabla_objetivo,
            async: false,
            dataType: 'json',
            username: uname,
            password: password
        }).done(function(response){
            console.log(response);
            if(response){
                catalogo = response;
            }
        }).fail(function(jqXHR, textStatus){
            console.log(jqXHR);
        });
        return catalogo;
    }

    // Función para obtener las reglas para los eventos
    function getCatalogoReglas(){
        let regla =false;
        $.ajax({
            method: 'GET',
            url: host + "event/ada_catalogo/nombre_tabla/regla",
            async: false,
            dataType: 'json',
            username: uname,
            password: password
        }).done(function(response){
            if(response){
                regla = response;
            }
        });
        return regla;
    }



    // Función para obtener los eventos de una actividad
    function getEventosActividad(grupo_id,tipo_actividad_id,actividad_id){
        let eventos =false;
        $.ajax({
            method: 'GET',
            url: host + "grupos/"+grupo_id+"/tipo_actividad/"+tipo_actividad_id+"/actividades/"+actividad_id+"/eventos",
            async: false,
            dataType: 'json',
            username: uname,
            password: password
        }).done(function(response){
            if(response){
                eventos = response;
            }
        }).fail(function(jqXHR, textStatus){
            console.log(jqXHR);
        });
        return eventos;
    }

    function getEventosCurso(grupo_id,curso_id){
        let eventos =false;
        $.ajax({
            method: 'GET',
            url: host + "cursos/"+curso_id+"/grupos/"+grupo_id+"/eventos",
            async: false,
            dataType: 'json',
            username: uname,
            password: password
        }).done(function(response){
            if(response){
                eventos = response;
            }
        }).fail(function(jqXHR, textStatus){
            console.log(jqXHR);
        });
        return eventos;
    }


    // Función para obtener los alumnos de un grupo 
    function getAlumnosCurso(grupo_id,tipo_actividad_id,actividad_id){
        let eventos =false;
        $.ajax({
            method: 'GET',
            url: host + "grupos/"+grupo_id+"/tipo_actividad/"+tipo_actividad_id+"/actividades/"+actividad_id+"/eventos",
            async: false,
            dataType: 'json',
            username: uname,
            password: password
        }).done(function(response){
            if(response){
                eventos = response;
            }
        }).fail(function(jqXHR, textStatus){
            console.log(jqXHR);
        });
        return eventos;
    }

   




/*
    *
    *     FUNCIÓN AJAX PARA EL POST
    * 
*/ 

function postEnviarRegla(data_evento){
    let post_regla = false;
    
    $.ajax({
        method: 'POST',
        url: host + "evento",
        async: false,
        dataType: 'json',
        data: data_evento,
        username: uname,
        password: password
    }).done(function(response){
        if(response){
            post_regla = response;
            
        }
    }).fail(function(jqXHR, textStatus){
        console.log(jqXHR);
    });
    
    return post_regla;
    
}
/*
    *
    *     FUNCIÓN AJAX PARA EL PUT
    * 
*/ 

function putEnviarRegla(data_evento){
    let put_regla = false;
    
    $.ajax({
        method: 'PUT',
        url: host + "evento",
        async: false,
        dataType: 'json',
        data: data_evento,
        username: uname,
        password: password
    }).done(function(response){
        if(response){
            put_regla = response;
            
        }
    }).fail(function(jqXHR, textStatus){
        console.log(jqXHR);
    });
    
    return put_regla;
    
}
function putCambiaEstadoEvento(evento_id, status){
    let actualizacion_estado = false;
    
    $.ajax({
        method: 'PUT',
        url: host + "evento/status",
        async: false,
        dataType: 'json',
        data: {
            evento_id,
            status
        },
        username: uname,
        password: password
    }).done(function(response){
        if(response){
            actualizacion_estado = response;
            
        }
    }).fail(function(jqXHR, textStatus){
        console.log(jqXHR);
        console.log(textStatus);
    });
    
    return actualizacion_estado;
    
}
