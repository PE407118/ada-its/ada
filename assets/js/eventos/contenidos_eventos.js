function generaColapsableEventos(id, unidad, actividad, unidad_id, actividad_id, tipo_actividad_id, contenido){
    let unidad_evento_clase='';
    let actividad_evento_clase='';
    let panel = '';
    if (unidad_id){
        unidad_evento_clase='unidad-evento';
        actividad_evento_clase='actividad-evento';
    }
    panel += '<div class="accordion" id="accordion_'+id+'" data-actividad="'+actividad_id+'" data-unidad="'+unidad_id+'">';
        panel += '<div class="card">';
            panel += '<div class="card-header" id="seccion_'+id+'" data-toggle="collapse" data-target="#collapse_'+id+'" aria-expanded="true" aria-controls="collapse_'+id+'">';
                panel += '<h2 class="mb-0">';
                    panel += '<button class="btn " type="button">';
                        panel += '<span class="'+unidad_evento_clase+'" data-unidad="'+unidad_id+'">'+unidad+'</span> / <span class="'+actividad_evento_clase+'" data-tipo-actividad="'+tipo_actividad_id+'" data-actividad="'+actividad_id+'">'+actividad+'</span>';
                    panel += '</button>';
                panel += '</h2>';
            panel += '</div>';

            panel += '<div id="collapse_'+id+'" class="collapse" aria-labelledby="seccion_'+id+'" data-parent="accordion_'+id+'">';
                panel += '<div class="card-body">';
                    panel += contenido
                panel += '</div>';
            panel += '</div>';
        panel += '</div>';
    panel += '</div>';
    return panel;
}