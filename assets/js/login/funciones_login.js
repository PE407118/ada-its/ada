
let login_form = null;
let alera_gneral = null;

inicializaPantalla();
function inicializaPantalla(){
    $('.alertas').addClass('oculto');
    login_form = new Formulario('section#login-form-container',rutas ['login'],$('form#login-form'),'POST',false);
    
    cargaAlertaGeneral();
}

function cargaAlertaGeneral(){
    let selector = '.mensajes_generales';
    alerta_general = new ElementoContenedor(selector);
    if ($(selector ).data('mensaje')!==''){
        alerta_general.enviaAlerta('danger', $(selector ).data('mensaje'));
    }
}

login_form.form.submit(function(event){
    login_form.enviaFormulario(event);
    alerta_general.ocultaAlerta();
});