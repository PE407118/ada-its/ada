class ElementoContenedor{
    constructor(contenedor){
        this.contenedor = contenedor;
        this.alerta = $(this.contenedor).find('.alertas');
    }
    enviaAlerta(tipo_alerta,mensaje){
        this.alerta.find('.alert').attr('class','alert');
        this.alerta.find('.alert').html(mensaje);

        this.alerta.find('.alert').addClass('alert-'+tipo_alerta);
        this.alerta.removeClass('oculto');
    }
    ocultaAlerta(){
        this.alerta.addClass('oculto');
        this.alerta.find('.alert').attr('class','alert');
        this.alerta.empty();
    }
    deshabilita(){
        this.ocultaAlerta();
    }
}

class Formulario extends ElementoContenedor{
    constructor( contenedor, destino, form,metodo_envio, prevent_default) {
        super(contenedor);
        this.destino = destino;
        this.form = form;
        this.metodo_envio = metodo_envio;
        if (this.metodo_envio == 'POST'){
            this.mensaje = '<p>No se guadardó la información ingresada.</p> ';
        }else{
            this.mensaje = '<p>No se actualizó la información ingresada.</p> ';
        }
        this.mensaje += '<p>Intente mas tarde o consulte con el administrador del sitio.';
        this.prevent_default = prevent_default;
    }

    enviaFormulario(event){
        
        
        //AQUÍ AGREGAR VALIDACIONES CUANDO SE TENGA EL MÉTODO
        let return_val = false;
        this.respuesta_ejecucion= peticion_ada_core.mandaPeticionPostPut(this.metodo_envio,this.destino,this.data_object);
        //console.log(this.respuesta_ejecucion);
        if (this.respuesta_ejecucion){
            let tipo_alerta = 'danger';
            if (this.respuesta_ejecucion['status']){
                tipo_alerta = 'success';
                return_val = true;
            }
            this.enviaAlerta(tipo_alerta,this.respuesta_ejecucion['message']);
        }else{
            this.enviaAlerta('warning',this.mensaje );
        }
        if (this.prevent_default ){
            if (!return_val){
                this.form[0].reset();
            }
            event.preventDefault();
        }
        if (!return_val){
            event.preventDefault();
        }
        
    }
    get data_object(){
        return this.calculaDataObject();
    }

    calculaDataObject(){
        return this.form.serializeArray().reduce(function(obj, item){
            if(item.value === ''){
                 obj[item.name] = undefined;
             } 
             obj[item.name] = item.value;
             return obj;
         }, {});
    }
    reiniciaForm(){
        this.form[0].reset();
    }
}